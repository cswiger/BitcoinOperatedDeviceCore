/*
 * BitcoinOperatedDeviceCore_vv.ino
 *
 *  Created on: 08.27.2018
 *
 *
 *
 */


/**** Custom configuration items ****/
// Put YOUR BTC testnet3 wallet address here
char waddr[] = "n3EdCJtov8rMR3BCJWtpQCtamYnu7faiKw";
// and YOUR WIFI ssid and passphrase here
char wifissid[] = "esquilo";
char wifipass[] = "frasspays";
/**** end of Custom configuration items ****/

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

#include <WebSocketsClient.h>

#include <ArduinoJson.h>
#include "Timer.h"

ESP8266WiFiMulti WiFiMulti;
WebSocketsClient webSocket;

Timer t;
uint8_t POWER = 16;   // Wemod D0 pin16 is POWER

void webSocketEvent(WStype_t type, uint8_t * payload, size_t length) {
    switch(type) {
        case WStype_DISCONNECTED:
            // Turn off blue LED when disconnected
            digitalWrite(LED_BUILTIN, HIGH);
            Serial.println("Disconnected!");
            break;
        case WStype_CONNECTED:
            // Turn on blue LED when connected
            digitalWrite(LED_BUILTIN, LOW);
            Serial.printf("Connected to url: %s\n\r",  payload);
                                    // send message to server when Connected, subscribe to your wallet address
            char subsc[64];
            strcpy(subsc,"{type: 'address',address:'");
            strcat(subsc,waddr);
            strcat(subsc,"'}");
                  webSocket.sendTXT(subsc);
            break;
        case WStype_TEXT:
            // received a websocket message, process it
            processJson(payload);
            break;
    }

}

void processJson(uint8_t * payload) {
    double value;
    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(payload);
    if (!root.success()) {
        Serial.println("parseObject() failed");
            return;
    }
    if (root["type"] == "heartbeat") return;
    if (root["type"] == "address") {
        // get number of outputs and check each one for our wallet
        uint8_t couts = root["payload"]["transaction"]["output_count"];
        for ( uint8_t i=0; i<couts; i++) {
            if (root["payload"]["transaction"]["outputs"][i]["addresses"][0] == waddr) {
                value = root["payload"]["transaction"]["outputs"][i]["value"];
            }
        }
        Serial.println(value,12);
        // you get 1 minutes for every 0.1mBTC
        double time = 60 * (value / 0.0001);
        Serial.printf("Setting timer for %u seconds\n\r",int(time));
        t.pulseImmediate(POWER, int(time) * 1000UL, HIGH);
    }
}

void setup() {
    Serial.begin(115200);
    // use an LED for connected state
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);

    pinMode(POWER,OUTPUT);       // setup POWER valve solid state relay pin
    digitalWrite(POWER,LOW);   //   and turn it OFF

    //Serial.setDebugOutput(true);

    Serial.println();

    Serial.println("addAP");
    WiFiMulti.addAP(wifissid, wifipass);

    //WiFi.disconnect();
    while(WiFiMulti.run() != WL_CONNECTED) {
        delay(100);
    }

    Serial.println(WiFi.localIP());
    Serial.println(WiFi.dnsIP());

    Serial.println("beginSSL testnet");
    webSocket.beginSSL("testnet-ws.smartbit.com.au", 443, "/v1/blockchain");
    Serial.println("registering event callback");
    webSocket.onEvent(webSocketEvent);

}

void loop() {
    webSocket.loop();
    t.update();
}

