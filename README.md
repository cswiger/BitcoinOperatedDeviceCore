Arduino sketch for the WEMOS D1 Mini (esp8266) to connect to a testnet3 monitor with websockets and watch for testnet3 BTC being send to a specific wallet address.
When the wallet being monitored received payment the D0 output is pulled HIGH to operate a solid state relay and power any device on, such as a fan, light, water valve, etc. 
